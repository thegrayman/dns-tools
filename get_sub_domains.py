#!/usr/bin/env python3

'''
Name: get_sub_domains.py
Author: Richard Gray <richard@rgray.info>

Description:
Finds all the subdomains for a given domain.

'''

import dns.resolver
import requests
import argparse

def get_dns_records(domain, record_type):
    try:
        answers = dns.resolver.resolve(domain, record_type)
        return [str(rdata) for rdata in answers]
    except Exception as e:
        return []

def get_subdomains_from_crtsh(domain):
    subdomains = set()
    url = f"https://crt.sh/?q=%25.{domain}&output=json"
    try:
        response = requests.get(url)
        if response.status_code == 200:
            data = response.json()
            for entry in data:
                name_value = entry.get('name_value')
                if name_value:
                    for subdomain in name_value.split('\n'):
                        if subdomain.endswith(f".{domain}"):
                            subdomains.add(subdomain)
    except Exception as e:
        print(f"Error retrieving subdomains from crt.sh: {e}")
    return subdomains

def brute_force_subdomains(domain, subdomains):
    known_subdomains = set()
    for subdomain in subdomains:
        full_domain = f"{subdomain}.{domain}"
        if get_dns_records(full_domain, 'A'):
            known_subdomains.add(full_domain)
    return known_subdomains

def main(domain):
    # Common subdomains. 
    common_subdomains = ['www', 'mail', 'ftp', 'api', 'blog', 'dev', 'test']

    # Find subdomains using crt.sh
    subdomains_from_crtsh = get_subdomains_from_crtsh(domain)
    print(f"Subdomains found from crt.sh: {subdomains_from_crtsh}")

    # Brute force common subdomains
    subdomains_from_bruteforce = brute_force_subdomains(domain, common_subdomains)
    print(f"Subdomains found from brute force: {subdomains_from_bruteforce}")

    # Combine results
    all_subdomains = subdomains_from_crtsh.union(subdomains_from_bruteforce)
    print(f"All subdomains: {all_subdomains}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Find subdomains for a given domain.')
    parser.add_argument('domain', type=str, help='The domain to find subdomains for.')
    args = parser.parse_args()
    main(args.domain)
